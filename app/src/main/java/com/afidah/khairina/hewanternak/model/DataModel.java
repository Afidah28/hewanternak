package com.afidah.khairina.hewanternak.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Khairina on 6/8/2018.
 */

public class DataModel {
    @SerializedName("id")
    private String mId;
    @SerializedName("nama_ternak")
    private String mnama_ternak;
    @SerializedName("kategori")
    private String mkategori;
    @SerializedName("jumlah")
    private String mjumlah;
    @SerializedName("pemilik")
    private String mpemilik;
    @SerializedName("harga")
    private String mharga;

    public String getId() {
        return mId;
    }
    public void setId(String id) {
        mId = id;
    }
    public String getnama_ternak() {
        return mnama_ternak;
    }
    public void setnama_ternak(String nama_ternak) {
        mnama_ternak = nama_ternak;
    }
    public String getkategori() {
        return mkategori;
    }
    public void setkategori(String kategori) {
        mkategori = kategori;
    }
    public String getjumlah() {
        return mjumlah;
    }
    public void setjumlah(String jumlah) {
        mjumlah = jumlah;
    }
    public String getpemilik() {
        return mpemilik;
    }
    public void setpemilik(String pemilik) {mpemilik = pemilik;}
    public String getharga() {
        return mharga;
    }
    public void setharga(String harga) { mharga = harga;}

}
