package com.afidah.khairina.hewanternak.Api;

import com.afidah.khairina.hewanternak.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Khairina on 6/8/2018.
 */

public interface RestApi {
    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponseModel> sendBiodata(@Field("nama_ternak") String nama_ternak,
                                    @Field("kategori") String kategori,
                                    @Field("jumlah") String jumlah,
                                    @Field("pemilik") String pemilik,
                                    @Field("harga") String harga);


    @GET("read.php")
    Call<ResponseModel> getBiodata();

    @FormUrlEncoded
    @POST("update.php")
    Call<ResponseModel> updateData( @Field("id") String id,
                                    @Field("nama_ternak") String nama_ternak,
                                    @Field("kategori") String kategori,
                                    @Field("jumlah") String jumlah,
                                    @Field("pemilik") String pemilik,
                                    @Field("harga") String harga);

    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponseModel> deleteData(@Field("id") String id);
}


