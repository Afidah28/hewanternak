package com.afidah.khairina.hewanternak;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afidah.khairina.hewanternak.Api.RestApi;
import com.afidah.khairina.hewanternak.Api.RetroServer;
import com.afidah.khairina.hewanternak.model.ResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText nama_ternak,kategori,jumlah,pemilik,harga;
    Button btnsave, btnTampildata, btnupdate,btndelete;
    ProgressBar pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pd= (ProgressBar)findViewById(R.id.pd);
        pd.setIndeterminate(true);
        pd.setVisibility(View.GONE);
        nama_ternak = (EditText) findViewById(R.id.edt_nama_ternak);
        kategori = (EditText) findViewById(R.id.edt_kategori);
        jumlah = (EditText) findViewById(R.id.edtjumlah);
        pemilik = (EditText) findViewById(R.id.edtpemilik);
        harga = (EditText) findViewById(R.id.edtharga);

        btnTampildata = (Button) findViewById(R.id.btntampildata);
        btnupdate =(Button) findViewById(R.id.btnUpdate);
        btnsave = (Button) findViewById(R.id.btn_insertdata);
        btndelete=(Button) findViewById(R.id.btnhapus);
//kondisi perubahan btn save > btn delete dan btn update
        Intent data = getIntent();
        final String iddata = data.getStringExtra("id");
        if(iddata != null) {
            btnsave.setVisibility(View.GONE);
            btnTampildata.setVisibility(View.GONE);
            btnupdate.setVisibility(View.VISIBLE);
            btndelete.setVisibility(View.VISIBLE);
            nama_ternak.setText(data.getStringExtra("nama_ternak"));
            kategori.setText(data.getStringExtra("kategori"));
            jumlah.setText(data.getStringExtra("jumlah"));
            pemilik.setText(data.getStringExtra("pemilik"));
            harga.setText(data.getStringExtra("harga"));
        }
//btn update
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setVisibility(View.VISIBLE);
                RestApi api =
                        RetroServer.getClient().create(RestApi.class);
                Call<ResponseModel> update =
                        api.updateData(iddata,nama_ternak.getText().toString(),kategori.getText().toString(),jumlah.getText().toString(),pemilik.getText().toString(),harga.getText().toString());
                update.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call,
                                           Response<ResponseModel> response) {
                        Log.d("Retro", "Response");
                        Toast.makeText(MainActivity.this,response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainActivity.this,
                                TampilData.class));
                        pd.setVisibility(View.GONE);
                        finish();
                    }
                    @Override
                    public void onFailure(Call<ResponseModel> call,
                                          Throwable t) {
                        pd.setVisibility(View.GONE);
                        Log.d("Retro", "OnFailure");
                    }
                });
            }
        });
//btn delete
        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setVisibility(View.VISIBLE);
                RestApi api =
                        RetroServer.getClient().create(RestApi.class);
                Call<ResponseModel> del = api.deleteData(iddata);
                del.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call,
                                           Response<ResponseModel> response) {
                        pd.setVisibility(View.GONE);
                        Log.d("Retro", "onResponse");
                        Toast.makeText(MainActivity.this,
                                response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        Intent gotampil = new
                                Intent(MainActivity.this,TampilData.class);
                        startActivity(gotampil);
                    }
                    @Override
                    public void onFailure(Call<ResponseModel> call,
                                          Throwable t) {
                        pd.setVisibility(View.GONE);
                        Log.d("Retro", "onFailure");
                    }
                });
            }
        });
        //btn tampil data
        btnTampildata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new
                        Intent(MainActivity.this,TampilData.class));
            }
        });
//button insert
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String snama_ternak = nama_ternak.getText().toString();
                String skategori = kategori.getText().toString();
                String sjumlah = jumlah.getText().toString();
                String spemilik = pemilik.getText().toString();
                String sharga = harga.getText().toString();

                if (snama_ternak.isEmpty() ) {
                    nama_ternak.setError("nama ternak perlu di isi");
                }else if (skategori.isEmpty()){
                    kategori.setError("kategori perlu di isi");
                }else if (sjumlah.isEmpty()){
                    jumlah.setError("jumlah perlu di isi");
                }else if (spemilik.isEmpty()){
                    pemilik.setError("pemilik perlu di isi");
                }else if (sharga.isEmpty()){
                    harga.setError("harga perlu di isi");
                }else {
                    RestApi api =
                            RetroServer.getClient().create(RestApi.class);
                    Call<ResponseModel> sendbio = api.sendBiodata(snama_ternak,skategori,sjumlah,spemilik,sharga);
                    sendbio.enqueue(new Callback<ResponseModel>() {
                        @Override
                        public void onResponse(Call<ResponseModel> call,
                                               Response<ResponseModel> response) {
/*
pd.setVisibility(View.GONE);
*/
                            Log.d("RETRO", "response : " +
                                    response.body().toString());
                            String kode = response.body().getKode();
                            if(kode.equals("1"))
                            {
                                Toast.makeText(MainActivity.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                                startActivity(new
                                        Intent(MainActivity.this, TampilData.class));
                                nama_ternak.getText().clear();
                                kategori.getText().clear();
                                jumlah.getText().clear();
                                pemilik.getText().clear();
                                harga.getText().clear();
                            }else
                            {Toast.makeText(MainActivity.this, "Data Error tidak berhasil disimpan", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseModel> call,
                                              Throwable t) {
                            Log.d("RETRO", "Falure : " + "Gagal Mengirim Request");
                        }
                    });
                }}
        });
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("warnig");
        alert.setMessage("do you wan to exit");
        alert.setPositiveButton("yes", new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int
                            i) {
                        MainActivity.this.finish();
                    }
                });
        alert.setNegativeButton("no", new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int
                            i) {
                    }
                });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }
}