package com.afidah.khairina.hewanternak.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afidah.khairina.hewanternak.MainActivity;
import com.afidah.khairina.hewanternak.R;
import com.afidah.khairina.hewanternak.model.DataModel;

import java.util.List;

/**
 * Created by Khairina on 6/8/2018.
 */

public class RecylerAdapter extends
        RecyclerView.Adapter<RecylerAdapter.MyHolder> {List<DataModel> mList ;
    Context ctx;
    public RecylerAdapter(Context ctx, List<DataModel> mList) {
        this.mList = mList;
        this.ctx = ctx;
    }
    @Override
    public RecylerAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist, parent, false);
        MyHolder holder = new MyHolder(layout);
        return holder;
    }
    @Override
    public void onBindViewHolder(RecylerAdapter.MyHolder holder,
                                 final int position) {
        holder.nama_ternak.setText(mList.get(position).getnama_ternak());
        holder.kategori.setText(mList.get(position).getkategori());
        holder.jumlah.setText(mList.get(position).getjumlah());
        holder.pemilik.setText(mList.get(position).getpemilik());
        holder.harga.setText(mList.get(position).getharga());
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {@Override
        public void onClick(View view) {
            Intent goInput = new Intent(ctx,MainActivity.class);
            try {
                goInput.putExtra("id", mList.get(position).getId());
                goInput.putExtra("nama_ternak", mList.get(position).getnama_ternak());
                goInput.putExtra("kategori", mList.get(position).getkategori());
                goInput.putExtra("jumlah", mList.get(position).getjumlah());
                goInput.putExtra("pemilik", mList.get(position).getpemilik());
                goInput.putExtra("harga", mList.get(position).getharga());
                ctx.startActivity(goInput);
            }catch (Exception e){
                e.printStackTrace();
                Toast.makeText(ctx, "Error data " +e, Toast.LENGTH_SHORT).show();
            }
        }
        });
    }
    @Override
    public int getItemCount()
    {
        return mList.size();
    }
    public class MyHolder extends RecyclerView.ViewHolder {
        TextView nama_ternak,kategori,jumlah,pemilik,harga;
        DataModel dataModel;
        public MyHolder(View v)
        {
            super(v);
            nama_ternak = (TextView) v.findViewById(R.id.tvnama_ternak);
            kategori = (TextView) v.findViewById(R.id.tvkategori);
            jumlah = (TextView) v.findViewById(R.id.tvjumlah);
            pemilik = (TextView) v.findViewById(R.id.tvpemilik);
            harga = (TextView) v.findViewById(R.id.tvharga);
        }
    }
}
