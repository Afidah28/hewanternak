-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Jun 2018 pada 16.10
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `hewanternak`
--

CREATE TABLE `hewanternak` (
  `id` int(11) NOT NULL,
  `nama_ternak` varchar(50) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `jumlah` varchar(50) NOT NULL,
  `pemilik` varchar(50) NOT NULL,
  `harga` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hewanternak`
--

INSERT INTO `hewanternak` (`id`, `nama_ternak`, `kategori`, `jumlah`, `pemilik`, `harga`) VALUES
(71, 'Ayam Kampung', 'Unggas', '150', 'Afi', '70000'),
(72, 'Burung Puyuh', 'Unggas', '10000', 'Afi', '30000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hewanternak`
--
ALTER TABLE `hewanternak`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hewanternak`
--
ALTER TABLE `hewanternak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
